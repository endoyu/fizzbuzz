/**
 *
 */
package jp.alhinc.yu_endo.fizzbuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author yuendo
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	/**
	 * {@link jp.alhinc.yu_endo.fizzbuzz.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_01() {
		System.out.println("testCheckFizzBuzz_01");
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}
	@Test
	public void testCheckFizzBuzz_02() {
		System.out.println("testCheckFizzBuzz_02");
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(3));
	}
	@Test
	public void testCheckFizzBuzz_03() {
		System.out.println("testCheckFizzBuzz_03");
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(5));
	}
	@Test
	public void testCheckFizzBuzz_04() {
		System.out.println("testCheckFizzBuzz_04");
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}
	@Test
	public void testCheckFizzBuzz_05() {
		System.out.println("testCheckFizzBuzz_05");
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}

}
